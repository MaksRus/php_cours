<?php

$string = "Coucou les gens !";

// Connaitre la taille d'une chaine de caractères
$taille = strlen($string);

//Trouver la position d'une lettre (première occurence) dans la chaine
$premierO = strpos($string, "o");
// Idem, mais trouve la dernière occurence
$dernierO = strrpos($string, "o");

// Inverse une chaine de caractères
$inversee = strrev($string);

// Passer une chaine de caractère en minuscule
$enMinuscules = strtolower($string);
// Idem, mais en majuscules cette fois
$enMajuscules = strtoupper($string);
// Met la première lettre de chaque mot en majuscule
$capitalized = ucwords($string);

// Remplace un mot
$plusPolie = str_replace("Coucou", "Bonjour", $string);

// Retourne une portion de la chaine (on précise le début et la fin)
$leDebut = substr($string, 0, 5); // "Couco"
$toutSaufLeDebut = substr($string, 5); // "u les amis"
echo "Le début : $leDebut\n";
echo "La fin: $toutSaufLeDebut\n";

// Vérifie si le chaine commence/fini par "X"
if (str_starts_with($string, "coucou")) {
    echo "effectivement !";
}
if (str_ends_with($string, "les gens !")) {
    echo "effectivement !";
}

// ATTENTION, un "echo" de HTML sera interprété
// par le navigateur !
$title =  "<h1>Je suis un titre</h1>";
echo $title; // "<h1>Je suis un titre</h1>" Peut être dangereux !

$titleSecur = htmlspecialchars($title);
echo $titleSecur; // "&lt1&gtJe suis un titre&lt/h1&gt"