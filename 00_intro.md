# PHP (Personal Home Page )

- Langage interprété côté "serveur" (donc "Back-end").

- Langage énormément utilisé dans le développement web.

- Souvent utilisé avec des bases de données relationnelles (MySql, MariaDb, SqLite...).

- Langage relativement simple à apprendre car il était destiné aux débutants.

- Sa facilité d'utilisation en fait un langage avec lequel on peut rapidement développer des applications web. Il est donc idéal pour les petites entreprises et les indépendants.

- PHP possède une "mauvaise" réputation mais n'y prêtez pas attention : le langage a BEAUCOUP évolué au fil des versions, est très performant, et offre tout ce que l'on est en droit d'attendre niveau POO (classes, propriétés privés, typage etc).

## Comment ça marche ?

Contrairement à nos langages "Front" qui sont lus par le navigateur, Php est lu par un serveur (par exemple, notre ordinateur).

1- Le Navigateur envoie une "requête" au serveur.

2- Notre code PHP se lance.

3- A la fin de notre programme, nous renvoyons une "réponse" contenant du HTML.

## Installation

- Se rendre sur [le site officiel PHP](https://www.php.net/downloads.php), trouver la version "Stable actuelle" et cliquer le "Windows download".

- A partir de là, prendre la version qui correspond à notre architecture système (32x, 64x).
Notez qu'il existe une version Thread Safe ou Non-Thread Safe : pour nous cela n'a aucune importance.

- Dézipper l'archive php et placer le contenu dans un dossier bien nommé (je vous conseille de le nommer "php+le-nom-de-votre-version", par exemple "php8.0.1").

- Créer un autre dossier qui s'appelera juste "php" que l'on placera à la racine de notre machine (C:).

- Mettre notre dossier "php8.0.1" (dans notre exemple) dans le dossier "php" qui est dans C.

- Appuyez sur la touche Windows pour accéder au menu et tapez : "variables".
Windows devrait vous proposer d'accéder aux "variables d'environnement système, allez y.

- Cliquez sur "Variables d'environnement" puis trouvez celle qui s'appelle le "Path". Modifions-la !

- Vous verrez alors plusieurs lignes, notre but est d'en ajouter une nouvelle avec le chemin vers notre version de php.
Dans notre exemple ce serait donc :
```
    C:\php\php8.0.1
```
Validez et fermez tout ça.

- Ouvrez à présent un terminal est tapez la commande :

```
   php --version 
```
Si votre version de php s'affiche, mission accomplie !

```diff
! Si votre IDE (VSCode, PhpStorm etc) était ouvert quand vous avez fait l'installation, vous devrez le relancer pour que php soit pris en compte !!
```