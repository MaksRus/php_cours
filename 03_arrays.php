<?php

//Simple Array (avec des index)
$numbers = [1, 2, 3, 4, 5];
$fruits = ['pomme', 'orange', 'poire'];

var_dump($numbers);
echo $fruits[0];

//Associative Array (avec des clés)
$student = [
    'name' => 'François',
    'age' => 36,
    'job' => 'developer',
];

echo $student['name']; // François

$colors = [
    'red' => '#F00',
    'green' => '#0F0',
    'blue' => '#00F',
];

// Bien sur, on peut avoir des tableaux de tableaux :

$students = [
    [
        'name' => 'François',
        'age' => 32,
        'job' => 'developer',
    ],
    [
        'name' => 'Charlotte',
        'age' => 36,
        'job' => 'engineer',
    ],
    [
        'name' => 'John',
        'age' => 26,
        'job' => 'office worker',
    ]
];

echo $students[0]['job']; // developer
