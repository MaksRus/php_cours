<?php

/* Exemple de classe en PHP */

/* Nous créons ici un "moule" Product qui va nous permettre d'instancier (générer) */
/* une infinité d'objets pré-formatés qui seront nos Produits. */
class Product
{

    /* Depuis PHP 8.0 on peut utiliser cette syntaxe pour nos  */
    /* constructeurs qui rappelle beaucoup celle de Typescript */

    /* J'estime que mes Produits ont tous 2 propriétés : un nom et un prix */
    /* Je peux bien sûr en rajouter ! */
    
    public function __construct(
        private string $name, // Ce n'est pas obligatoire mais je peux typer ces propriétés (FAITES-LE !!)
        private float $price,
    ){}

    /* Comme toutes mes propriétés sont "private" (privées) il est nécéssaire de créer */
    /* des Getter/Setter pour chacune d'entre elle */
    /* TIP : Si vous utilisez une extension VSCode ou, encore mieux, PhpStorm, vous pouvez "generate Getter/Setter" */

    public function getName(): string
    {
        // le mot clé THIS permet à notre Product de se référencer lui même.
        // Ici, on demande à notre produit de nous donner son nom
        // Le retour sera donc différent SELON le produit que nous solicitons
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    // Nous avons parfois besoin de connaitre le prix en centimes
    // de nos produit.
    // Nous pouvons donc créer une méthode qui retourne cette information
    //
    // Concrêtement, nous venons d'apprendre à nos Product comment ils peuvent eux-mêmes
    // convertir leur propre prix !
    public function getPriceAsCents(): int
    {
        return $this->price * 100;
    }

    // Il existe en php des méthodes "magiques" (oui oui, c'est le vrai nom)
    // la méthode __toString se lance automatiquement si l'on essaye d'utiliser
    // un objet comme une chaîne de caractères (avec un echo par exemple).
    public function __toString(): string
    {
        // Si cela arrive, notre Product dira juste son nom
        // Ainsi on évite une erreur !
        return $this->name;
    }
}

/* ---------------------------------------------------------------------- */
/* -----------------------   Instanciation   ---------------------------- */
/* ---------------------------------------------------------------------- */

$product = new Product("Ordinateur", 799.99); // On créé un premier Produit en passant au constructeur le nom et le prix
$product2 = new Product("Brosse à dents", 5.99); // On peut créé autant de produit que l'on veut

echo $product->getName(); // "Ordinateur"
echo $product2->getName(); // "Brosse à dents"

// Chaque Product peut lui même calculer son prix en centimes si besoin grâce à la méthode 
// que nous lui avons créé
echo $product->getPriceAsCents(); 

?>







<?php include "partials/header.php" ?>
<main>
    Coming soon ...
</main>
<?php include "partials/footer.php" ?>
