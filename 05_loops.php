<?php

// Boucle For classique
for ($i = 0; $i < 10; $i++) {
    echo $i . "\n";
}

$notes = [12, 38, 10, 30, 21];
/**
 * On "boucle" sur notre tableau "notes"
 * "Note" au singulier représente chaque élément
 * individuel de notre tableau mais n'est accessible que dans ce bloc
 */
foreach($notes as $note) {
    echo $note;
}

// Foreach and générant un tableau grace à "range()"
foreach(range(1, 10) as $num) {
    echo $num + "\n";
}

$nbDeTours = 0;
// La boucle se répète "tant que" la condition se valide
while($nbDeTours < 10) {
    echo "On refait un tour ! \n";
    $nbDeTours++;
}

// Avec "do" on est certain de rentrer au moins 
// une fois dans la boucle.
do {
    echo "On refait un tour ! \n";
    $nbDeTours++;
} while ($nbDeTours < 10);