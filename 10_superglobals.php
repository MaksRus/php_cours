<?php
/* ---------- Superglobals ---------- */
// Des "Super variables" accessibles depuis n'importe où

/*
  $GLOBALS - Une "superglobal" qui contient des infos sur toutes les variables 
  $_GET - Contient les infos sur les variables passées depuis L'URL ou un Formulaire 
  $_POST -  Contient les infos sur les variables passées depuis un Formulaire 
  $_COOKIE - Contient les infos sur les cookies 
  $_SESSION - Contient les variables de session
  $_SERVER - Contient les infos sur le serveur
  $_ENV - Contient les variables d'environnement
  $_FILES - Contient les infos sur les fichers chargés dans le script
  $_REQUEST - Contient les infos sur la Requête HTML et les Formulaires
*/

// var_dump($GLOBALS);
// var_dump($_GET);
// var_dump($_REQUEST);
?>
<?php include "partials/header.php" ?>
<main>
  <ul>
    <li>Host: <?php echo $_SERVER['HTTP_HOST']; ?></li>
    <li>Document Root: <?php echo $_SERVER['DOCUMENT_ROOT']; ?></li>
    <li>System Root: <?php echo $_SERVER['SystemRoot']; ?></li>
    <li>Server Name: <?php echo $_SERVER['SERVER_NAME']; ?></li>
    <li>Server Port: <?php echo $_SERVER['SERVER_PORT']; ?></li>
    <li>Current File Dir: <?php echo $_SERVER['PHP_SELF']; ?></li>
    <li>Request URI: <?php echo $_SERVER['REQUEST_URI']; ?></li>
    <li>Server Software: <?php echo $_SERVER['SERVER_SOFTWARE']; ?></li>
    <li>Client Info: <?php echo $_SERVER['HTTP_USER_AGENT']; ?></li>
    <li>Remote Address: <?php echo $_SERVER['REMOTE_ADDR']; ?></li>
    <li>Remote Port: <?php echo $_SERVER['REMOTE_PORT']; ?></li>
  </ul>
</main>
<?php include "partials/footer.php" ?>