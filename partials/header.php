<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cours PHP</title>
    <link rel="stylesheet" href="/assets/css/style.css">
</head>

<body>
    <header>
        <h1>Php</h1>
    </header>
    <nav>
        <ol>
            <li><a href="/01_output.php">Les outputs</a></li>
            <li><a href="/02_variables.php">Les variables</a></li>
            <li><a href="/03_arrays.php">Les tableaux</a></li>
            <li><a href="/04_conditions.php">Les conditions</a></li>
            <li><a href="/05_loops.php">Les boucles</a></li>
            <li><a href="/06_functions.php">Les fonctions</a></li>
            <li><a href="/07_array_functions.php">Fonctions des tableaux</a></li>
            <li><a href="/08_string_functions.php">Fonctions des trings</a></li>
            <li><a href="/09_view.php">Affichage</a></li>
            <li><a href="/10_superglobals.php">Les superglobales</a></li>
            <li><a href="/11_get_post.php">Get / Post</a></li>
            <li><a href="/12_sanitizing_inputs.php">Filter les inputs</a></li>
            <li><a href="/13_cookies.php">Les cookies</a></li>
            <li><a href="/14_sessions.php">Les sessions</a></li>
            <li><a href="/15_file_handling.php">Les fichiers</a></li>
            <li><a href="/16_file_upload.php">Upload de fichiers</a></li>
            <li><a href="/17_exceptions.php">Les exceptions php</a></li>
            <li><a href="/18_oop.php">La POO</a></li>
        </ol>
    </nav>