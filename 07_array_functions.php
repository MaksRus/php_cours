<?php

$fruits = ["cerise", "pêche", "pomme"]; 

// Savoir combien il y a d'élément
count($fruits);

// Savoir si un élément se trouve dans le tableau
in_array("pêche", $fruits);

// Ajouter un élément
$fruits[] = "abricot"; // Version courte
// Version longue qui permet d'ajouter autant d'éléments que l'on veut d'un seul coup
array_push($fruits, "poire", "myrtille");
$fruits[] = "prune";
array_unshift($fruits, "fraise"); // Placera "fraise" au DEBUt du tableau

// Enlever des éléments
array_pop($fruits); // Supprime le dernier élément
array_shift($fruits); // Supprime le premier élément
unset($fruits[2]); // Supprime l'élément à l'index 2

// Découpe notre tableau en 2 (ou plus) parties
$chunked_array = array_chunk($fruits, 2);

$tableau1 = [1, 2, 3];
$tableau2 = [4, 5, 6];

// Fusionne plusieurs tableaux ensemble
$tableau3 = array_merge($tableau1, $tableau2);


// Fusionne 2 tableaux afin d'en créer un associatif
// Les éléments du tableau1 deviennent les clés
// Les éléments du tableau2 deviennent les valeurs
$infoDemandees = ['nom', 'prenom', 'age'];
$infoRecuperees = ['Dupond', 'Bernard', 48];
$toutesLesInfos = array_combine($infoDemandees, $infoRecuperees);

$student = [
    'nom' => "Bernard",
    'age' => 48,
    'email' => 'bernard@gmail.fr'
];

// Construit un tableau ne contenant que les clés d'un autre tableau
$cles = array_keys($student);

// Inverser clés/valeurs
$inverse = array_flip($student);

// Générer un tableau contenant des nombres de n1 à n2
$nombres = range(1, 20);

// Fais un "foreach" sur un tableau afin d'en générer
// un nouveau avec les valeurs de retour
$double = array_map(function($nombre) {
    return $nombre *2;
}, $tableau1);

// Filtrer un tableau
// Ici, on ne garde de "nombres" que ceux qui sont au moins égaux à 10
$bigNumbers = array_filter($nombres, function($nombre) {
    return $nombre >= 10;
});

// Additionne toutes les valeurs d'un tableau
$total = array_sum($nombres);
