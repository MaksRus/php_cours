<?php
$messages = [];

if(isset($_POST['submit'])) {
  $allowed_ext = ['png', 'jpg', 'jpeg'];
  // print_r($_FILES);
  
  if (!empty($_FILES['avatar']['name'])) {
    $file_name = $_FILES['avatar']['name'];
    $file_size = $_FILES['avatar']['size'];
    $file_tmp = $_FILES['avatar']['tmp_name'];

    $target_dir = "uploads/${file_name}";
    // On récupère l'extension du fichier
    $file_ext = explode('.', $file_name);
    $file_ext = strtolower(end($file_ext));


    if (in_array($file_ext, $allowed_ext)) {
      if ($file_size <= 10000000) {
        move_uploaded_file($file_tmp, $target_dir);
        $messages[] = "Merci ! Votre avatar a été téléchargé avec succès !";

      } else {
        $messages[] = "Le fichier est trop volumineux !";

      }
    } else {
      // Ce type de fichier n'est pas accepté par notre configuration 
      $messages[] = "Nous n'acceptons que les images jpg/png !";

    }

  } else {
    $messages[] = "Choisissez un fichier svp.";
  }

}
?>

<?php include "partials/header.php" ?>
<main>
    <?php foreach ($messages as $msg) : ?>
      <p><?= $msg ?></p>
    <?php endforeach ?>

    <form 
      action="<?= $_SERVER['PHP_SELF']?>" 
      method="POST" 
      enctype="multipart/form-data">

      <label for="file">Avatar</label>
      <input type="file" name="avatar">

      <input type="submit" name="submit">
    </form>
</main>
<?php include "partials/footer.php" ?>
