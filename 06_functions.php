<?php

// On définie notre fonction
function sum($a, $b) {
    return $a + $b;
}

// sum(30, 20); // On utilise la fonction

function sub(float $a, float $b): float {
    return $a - $b;
}

// Les fonctions fléchées existent aussi mais sont
// beaucoup moins utilisées qu'en Javascript et manquent de lisibilité ! 
$multiply = fn($a, $b) => $a * $b;

// $multiply(5, 8);

$age = 18;
$progression = 10;

$notes = [1, 2, 3, 4];

// Fonction anonyme qui utilise une variable "externe"
$total = array_map(function($note) use ($age) {
    return $note + $age;
}, $notes);

// Fonction qui modifie un paramètre sans avoir besoin de le retourner
function grandir(&$annees) {
    $annees++;
}

grandir($age);
echo $age;