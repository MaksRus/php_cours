<?php 


// Une classe "abstraite" ne peut pas être instanciée.
abstract class Book
{
    public function __construct(
        // On met les propriétés en "protected"
        // afin que les enfants de cette classe puissent les utiliser aussi
        protected string $title,
        protected string $author,
    ){}

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function __toString(): string
    {
        return "Mon titre est $this->title, j'ai été écrit par $this->author."; 
    }
}


// On peut créer une classe fille "Livre Physique" qui étend de Book
// De cette façon, PhysicalBook hérite de l'intégralité des propriétés
// et méthode de sa mère.
class PhysicalBook extends Book
{
    public function __construct(
        string $title,
        string $author,
        // On en profite pour rajouter la propriété "Weight" !
        // Et oui ! Seuls les lives physiques ont un "poids".
        private int $weight,
    ){
        // On appelle le constructeur du parent (Book) en lui
        // passant ce dont les Book ont besoin : un titre et un auteur
        parent::__construct($title, $author);
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function setWeight(int $weight): void
    {
        $this->weight = $weight;
    }
    
    // J'en profite pour 'override' la méthode __toString de la mère Book
    // Je veux que mon livre physique fasse la même chose que sa mère MAIS 
    // précise également son poids en grammes !
    public function __toString(): string
    {
        return parent::__toString() . " et je pèse $this->weight gr."; 
    }
}

// Je créé une 2ème variation : le DigitalBook
// Lui aussi est un "Book" mais conctrairement au PhysicalBook il n'a pas
// de poids en grammes mais une "taille de fichier" (filesize).
class DigitalBook extends Book
{
    public function __construct(
        string $title,
        string $author,
        private int $filesize,
    ){
        parent::__construct($title, $author);
    }

    public function getFilesize(): int
    {
        return $this->filesize;
    }

    public function setFilesize(int $filesize): void
    {
        $this->filesize = $filesize;
    }

    public function __toString(): string
    {
        return parent::__toString() . " et je pèse $this->filesize ko ."; 
    }
}

/* ---------------------------------------------------------------------- */
/* -----------------------   Instanciation   ---------------------------- */
/* ---------------------------------------------------------------------- */

$book = new PhysicalBook("The Lord of the Rings", "Tolkien", 400);
$bookNumerique = new DigitalBook("Harry Potter", "J.K. Rowling", 1200);

// Nos 2 livres ont un titre et un auteur, puisque ce sont des Book
echo $book->getTitle();
echo $bookNumerique->getTitle();

//MAIS seul notre Livre physique possède un poids en grammes
echo $book->getWeight();

// De même, seule le livre numérique possède une "taille de fichier", logique !
echo $bookNumerique->getFilesize();

