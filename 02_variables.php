<?php

/*
 Les types de données en PHP
  - String
        Chaine de caractère ("", '')
  - Integer
        Nombres entiers
  - FLoat
        Nombres décimaux
  - Bollean
        true ou false (vrai ou faux)
  - Array
        Les tableaux []
  - Object
        Les objets (instances de classes)
  - NULL
        Valeur nulle, vide
  - Resource
        Type spécial représentant une "resource" (ex: un fichier)
*/

/*
    Les Variables en PHP
- Les variables commencent toutes par un "$"
- Le nom de la variable doit commencer par une lettre ou un _
- Pas de caractères speciaux !
- Les variables sont "case-sensitive"
($prenom et $PRENOM sont des variables différentes)
 */

$name = "Micheline"; // string
$age = 42; // int 
$balance = 242.75; // float
$isStudent = true; // bool

// Racourcis
$number = 5;

$number += 2; // On ajoute 2 au contenu de notre variable
$number -= 2; // On soustrait 2 au contenu de notre variable 
$number *= 2; // On multiplie par 2 le contenu notre variable
$number /= 2; // On divise par deux le contenu de notre variable

$number++; // Maintenant "$number" est à 6
$number--; // Maintenant "$number" est à 5

//Cela marche aussi pour la concaténation des chaines de caractères
$greetings = "Bienvenue";
$greetings .= "!"; // Résultat : "Bienvenue!"

// Constantes
define('PASSWORD', "mdp1234");
echo PASSWORD;
?>

<?php include "partials/header.php" ?>
<main>
      <h1>
            Les types de données en PHP
      </h1>
      <ul>
            <li>
                  - String
                  Chaine de caractère ("", '')
            </li>
            <li>
                  - Integer
                  Nombres entiers
            </li>
            <li>
                  - FLoat
                  Nombres décimaux
            </li>
            <li>
                  - Bollean
                  true ou false (vrai ou faux)
            </li>
            <li>
                  - Array
                  Les tableaux []
            </li>
            <li>
                  - Object
                  Les objets (instances de classes)
            </li>
            <li>
                  - NULL
                  Valeur nulle, vide
            </li>
            <li>
                  - Resource
                  Type spécial représentant une "resource" (ex: un fichier)
            </li>
      </ul>
      <?php
      echo $name;
      echo $age;
      var_dump($isStudent);

      // Concatenation

      // Avec des guillemets simples : '' 
      echo '$name a $age ans.'; // Résultat '$name a $age ans'

      echo $name . ' a ' . $age . ' ans.'; // Résultat 'Micheline a 42 ans'

      echo "\n";

      // Avec des guillemets doubles : "" 
      echo "$name a $age ans \n."; // Résultat 'Micheline a 42 ans'

      echo "${name} a ${age} ans. \n"; // Idem, mais un peu plus propre et lisible

      ?>
</main>
<?php include "partials/footer.php" ?>