<?php

$file = 'resources/users.txt';

// Outils permettant de trouver le Path vers un fichier:
// __DIR__ -> constante qui retourne le chemin vers le dossier
// dirname($file) -> fonction qui retourne le chemin du dossier dans lequel se trouve le fichier fourni
// DIRECTORY_SEPARATOR -> constante qui contient "/" ou "\" selon votre systême d'exploitation (Windows: '\' - Linux: '/')

//---------------------------------------------------------------------------------
// La méthode "simple"
//---------------------------------------------------------------------------------

// LECTURE
$content = file_get_contents($file);
echo $content;

// ECRITURE
$newContent = "Coucou je suis nouveau !";
$succes = @file_put_contents($file, $newContent);
if (!$succes) {
    echo "Impossible d'écrire le fichier !";
} else {
    echo "Fichier mis à jour !";
}
// Cette méthode récupère l'intégralité du contenu du fichier dans la RAM et n'est donc
// valable qu'avec de petits fichiers ! Mais elle a l'avantage d'être simple à manipuler.


//---------------------------------------------------------------------------------
// La méthode plus précise 
//---------------------------------------------------------------------------------

/* Recap des fonctions utiles : */

/*
 * fopen($fichier) : ouvre un fichier et retourne la "resource" 
 * -- on choisit un "mode : 
 * ---- r (lecture)
 * ---- w (écriture (on remplace le contenu existant))
 * ---- a (ajout (on écrit APRES le contenu existant))
 * fread($resource) : permet de lire le contenu de la resource
 * fgets($resource) : permet de lire UNE ligne de la resource
 * fwrite($resource, $contenu ) : permet d'écrire dans notre resource 
 *
 * fseek($resource, $deplacement) : pour déplacer notre "curseur" dans la resource
 * fstat($resource) : pour récupérer des informations sur la resource
 *
 * fclose($fichier) : Ne pas oublier de "refermer" le fichier après utilisation
 */

/* Lire un fichier */
if (file_exists($file)) {
    // On passe l'option "r" (read)
    $handle = fopen($file, 'r');
    // Php n'ai pas content quand on lui donne une taille de 0 ce qui peut arriver avec un fichier très léger
    $filesize = filesize($file) > 0 ? $filesize($file) : 1;
    echo  $filesize;

    // On récupère le contenu dans "contents"
    $contents = fread($handle, $filesize);

    // Ou, on peut utiliser fgets pour traiter le contenu ligne par ligne !
    /* while ($line = fgets($handle)) { */
    /*     echo $line; */
    /* } */

    // On oublie pas de fermer notre fichier
    fclose($handle);
    echo $contents;

} else {
    // Ecrire un fichier
    // On passe l'option "w" (write)
    $handle = fopen($file, "w");

    // Variante : on peut aussi "ajouter" du contenu à un fichier avec l'option "a" (append)
    // $handle = fopen($file, "a");

    // On définit le contenu que l'on veut écrire
    $contents = "Charlotte" . PHP_EOL . "Michel" . PHP_EOL;
    fwrite($handle, $contents);
    // On oublie pas de fermer notre fichier
    fclose($handle);
}

?>

