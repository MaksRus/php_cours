<?php 

// Si vous voulez utiliser la session, votre code
// doit commencer par cette fonction session_start
session_start();

// A partir d'ici, nous avons accès à la superglobal
// $_SESSION dans laquelle nous pouvons ranger tout ce que
// l'on veut

$_SESSION['user'] = [
    "username" => "Charlotte",
    "isAdmin" => true,
];

$_SESSION['nb_de_frites'] = 42;
$_SESSION['virus'] = false;

// Cette variable de session est maintenant
// disponible partout dans l'application !

// Il faudra juste bien penser à vérifier l'EXISTENCE
// des variables que l'on veut utiliser
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    echo "Bonjour " . $user['username'];
} else {
    echo "Bonjour visiteur.";
}

// Pour détruire une session (par exemple, déconnecter notre user)
// on utilise ceci :
    $_SESSION = []; //Par sécurité mais pas obligatoire
    session_destroy();

//A noter que pour faire un "session destroy" il faut d'abord
// avoir fait un session_start !
