# Session

- Faire un nouveau projet

- Le projet devra contenir 5 fichiers
    - index.php
    - contact.php
    - forum.php
    - shop.php
    - gallery.php

- Vous pouvez (devriez !!) également créé une partial "header.php" qui contiendra
la navigation entre ces différentes pages.

- L'objectif est d'espionner notre visiteur : 
sur la page d'accueil, nous devons afficher COMBIEN de fois l'utilisateur
a visité chacune des pages.

Exemple : 
    Je clique sur "Contact", puis sur "Forum", puis sur "Contact" à nouveau.
    Quand je reviens sur la page d'accueil, il doit y avoir affiché : 
        Contact: 2
        Forum: 1
        Shop: 0
        Gallery: 0
