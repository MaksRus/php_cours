# Include

- Objectif : scinder le code HTML et PHP de son site en plusieurs parties.

- Vos "pages" devraient être à la racine de votre projet.
Vous pouvez créér un dossier "pages" mais gardez bien votre index.html à la racine.

- Les "components" (ou "partials") tels que le header ou le footer doivent être rangée
dans un sous dossier.

- Le but étant de n'avoir jamais à se répéter et de simplement "importer" les
composants là où l'on en a besoin.

- ***Veillez à garder votre "LOGIQUE" en haut des pages PHP et la "VUE" en dessous 👍 !***

Bonus : Faire en sorte que le "titre" de chaque page soit différent (cad le nom de la
page tel qu'il est indiqué dans l'onglet de votre navigateur).