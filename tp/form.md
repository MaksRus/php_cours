# Les Formulaires

- Réaliser un formulaire (GET ou POST, au choix) permettant de récupérer un
maximum d'information sur l'utilisateur.

- Le but étant d'utiliser une grande variété d'inputs différents, il faudra donc
penser à inclure un moins un textarea, des checkbox, des radios et un select.

- Si le formulaire a été rempli, on vérifie les informations envoyées et on les affiches
de façon claire dans la vue (sur la même page).
Exemple : 
    "Bonjour $name, vous avez $age ans et votre pokemon préféré est $favoritePokemon".

- Si vous avez le temps, essayez de générer des messages d'erreurs en cas d'informations
non/mal-saisies.

Exemple:
    "Attention, le mot de passe doit contenir au moins une majuscule."