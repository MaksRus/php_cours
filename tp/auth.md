# Authentification

- Ajouter à son site une page "Connexion" avec un formulaire en POST.

- L'utilisateur devra rentrer son email et un mot de passe afin de s'authentifier.

- Vous utiliserez pour cela le tableau des "users".

- Le traitement du formulaire devra se faire sur une page "action" dédiée et
renvoyer sur la page d'accueil en cas d'authentification reussie ou bien
sur la page de connexion.

- Faire une action "logout.php" qui permet de se deconnecter

- Si l'utilisateur s'est authentifié avec succès, il faudra lui créer une session
et adapter la vue avec un message d'accueil approprié ("Bonjour Sylvain !").

- Adapter la navigation à la situation : 
    On propose un lien vers la page de connexion que si l'utilisateur n'est PAS connecté
    On propose de se deconnecter seulement aux utilisateurs connectés

- Si l'utilisateur est "admin", il devra en plus avoir accès à une page "administration".
Cette page doit être interdite d'accès aux visiteurs et aux utilisateurs lambda !