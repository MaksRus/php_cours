# Pagination (easy mode)

- Créer une page "archive.php"

- Cette page doit afficher 2 choses : 
    - le numéro de la page actuelle
    - 5 boutons/liens de pagination numérotés (1, 2, 3, 4, 5)

- L'objectif : quand on clique sur l'un des boutons/liens, nous devons
toujours être sur archive.php, MAIS le contenu de la page doit avoir changé.

Exemple :
    Je clique sur le bouton "3", j'arrive sur archive.php mais il y a écris "PAGE 3"
    Je clique sur le bouton "5", j'arrive sur archive.php mais il y a écris "PAGE 5"

A vous de trouver comment faire !