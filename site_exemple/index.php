<?php
  session_start();

  if (!isset($_SESSION['visites'])) {
    $_SESSION['visites'] = [
      'articles' => 0,
      'form' => 0,
      'contact' => 0,
    ];
  }



  $title = "Accueil";
  include 'partials/header.php';
  ?>
  <main>
    <h1>Bienvenue !</h1>
    <article>
      Article
    </article>
    <section>
      Section
    </section>
  </main>

  <?php if (isset($_SESSION['visites'])) : ?>
    Articles : <?= $_SESSION['visites']['articles'] ?>
    Form : <?= $_SESSION['visites']['form'] ?>
    Contact : <?= $_SESSION['visites']['contact'] ?>
  <?php endif ?>
  <?php include 'partials/footer.php' ?>