<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?? "Aformac" ?></title>
</head>

<body>
    <header>
        <nav>
            <ul>
                <li><a href="/">Accueil</a></li>
                <li><a href="/articles.php">Articles</a></li>
                <li><a href="/contact.php">Contact</a></li>
                <li><a href="/form.php">Form</a></li>
                <!-- On ne peut se deconnecter que SI on est connecté ! -->
                <?php if (isset($_SESSION['user'])) : ?>
                    <li><a href="/actions/logout.php">Deconnexion</a></li>
                    <?php if ($_SESSION['user']['isAdmin']) : ?>
                        <!-- Seul l'admin peut voir ce lien -->
                        <li><a href="/admin.php">Administration</a></li>
                    <?php endif ?>
                    <?php else : ?>
                        <!-- Si le visiteur n'est pas connecté, on lui propose -->
                    <li><a href="/connexion.php">Connexion</a></li>
                <?php endif ?>
            </ul>
        </nav>
    </header>
    <?php if (isset($_SESSION['user'])) : ?>
        <h2>Bonjour <?= $_SESSION['user']['username'] ?></h2>
    <?php endif ?>