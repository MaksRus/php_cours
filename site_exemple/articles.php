<?php
session_start();
$_SESSION['visites']['articles']++;
// La logique 

require_once 'functions/heroes.php';

// Par défaut, on dit qu'on est sur la page 1
$page = 1;

// MAIS si l'information est présente dans l'URL (localhost/articles.php?page=3),
// on met à jour
if (isset($_GET['page'])) {
    $page = htmlspecialchars($_GET['page']);
}

$articles = [];
$title = "Tous les articles";
?>

<!-- View -->
<?php include 'partials/header.php' ?>
<h1>Tous nos articles</h1>

<!-- On affiche le numéro de la page -->
<h2>Vous êtes sur le page numéro <?= $page ?></h2>
<ul>
    <?php foreach ($articles as $article) : ?>
        <?php include 'partials/article-item.php' ?>
    <?php endforeach ?>
</ul>

<!-- Génération automatisée de liens vers les différentes pages -->
<ul style="display: flex; gap: 2rem; list-style: none;">
    <?php foreach (range(1, 5) as $index) : ?>
        <!-- On génère des liens qui pointent tous sur "articles.php" mais avec un paramètre "page" différent -->
        <li><a href="/articles.php?page=<?= $index ?>"><?= $index ?></a></li>
    <?php endforeach ?>
</ul>

<?php include 'partials/footer.php' ?>