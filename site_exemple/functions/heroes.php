<?php

function createHero(string $name, int $hp): array 
{
    return [
        'name' => $name,
        'hp' => $hp
    ];
}