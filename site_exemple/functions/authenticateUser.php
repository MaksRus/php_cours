<?php

/**
 * Find a user by Email and Password
 *
 * @param string $email
 * @param string $password
 * @return array
 */
function authenticateUser(string $email, string $password): array
{
    // J'importe ma liste des Users pour pouvoir l'exploiter
    include '../resources/users.php';

    // On recherche notre utilisateur
    foreach ($users as $user) {
        // Première vérif ! Est-ce qu'on a quelqu'un avec cet email ?
        if ($user['email'] === $email) {
            // Si on trouve, on vérifie son mdp aussi bien sûr
            if ($user['password'] === $password) {
                return $user;
            }
        }
    }
    // Si on ne trouve rien, on retourne du vide
    return [];
}
