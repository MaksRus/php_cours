<?php session_start() ?>

<?php include 'partials/header.php' ?>
<form action="/actions/login.php" method="POST">
    <!-- On affiche un message d'erreur en cas de pépin -->
    <?php if(isset($_SESSION['errors'])): ?>
        <?=$_SESSION['errors']?>
        <!-- Je supprime ensuite le message d'erreur (sinon il va apparaitre à chaque fois !)-->
        <?php unset($_SESSION['errors']) ?>
    <?php endif ?>
    <div>
        <label for="email">Email</label>
        <input type="email" name="email" id="email" required>
    </div>
    <div>
        <label for="password">password</label>
        <input type="password" name="password" id="password">
    </div>
    <input type="submit" name="submit">
</form>
<?php include 'partials/footer.php' ?>