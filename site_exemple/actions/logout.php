<?php 
session_start(); // <-- même si c'est pour la détruire, il faut lancer la session !

// On détruit toutes les données de session
$_SESSION = []; // <-- pas obligatoire
session_destroy();

// Redirection sur l'accueil
header('Location: /');
?>