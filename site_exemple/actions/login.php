<?php
// Ne PAS oublier le session_start ^^
session_start();

// Je me suis créé une fonction qui se charge
// de la recherche et de l'authentification
// Je l'importe o/ !
require_once '../functions/authenticateUser.php';

// Seulement si le formulaire a été soumis
if (isset($_POST['email']) && isset($_POST['password']) && $_POST['password'] != "") {
    // On récupère les données du formulaire et on les traite
    $email = htmlspecialchars($_POST['email']);
    $password = htmlspecialchars($_POST['password']);

    // Maintenant je cherche un User qui a cet email ET ce mdp
    $user = authenticateUser($email, $password);
    
    if (!empty($user)) {
        // On a un User ! Tout va bien !
        // Pour s'en souvenir, on le garde en SESSION
        $_SESSION['user'] = [
            'username' => $user['name'],
            'isAdmin' => $user['isAdmin'],
        ];

        header('Location: /');
        exit;
    }
}

// Si le formulaire n'est pas valide ou que l'on trouve
// pas de User, on redirige sur le formulaire avec une erreur
// en SESSION (pour pouvoir la récupérer sur la page suivante)
    $_SESSION['errors'] = "Identifiants invalides !";
    header('Location: /connexion.php');