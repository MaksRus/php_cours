<?php
session_start();
$_SESSION['visites']['form']++;
$errors = [];

if (isset($_POST['submit'])) {
    if (!isset($_POST['pseudo']) || $_POST['pseudo'] == "") {
        $errors[] = "Il faut nous donner un pseudo !";
    }
    $pseudo = htmlspecialchars($_POST['pseudo']);
    $age = htmlspecialchars($_POST['age']);

    echo $pseudo;
}

?>
<?php include 'partials/header.php' ?>
<form action="/actions/register.php" method="POST">
    <div>
        <label for="pseudo">Pseudo</label>
        <input type="text" name="pseudo" id="pseudo" required>
    </div>

    <div>
        <label for="age">Age</label>
        <input type="number" name="age" id="age">
    </div>

    <div>
        <label for="password">password</label>
        <input type="password" name="password" id="password">
    </div>
    <input type="submit" name="submit">
</form>
<?php include 'partials/footer.php' ?>