<?php
 
 session_start();
 // Si on a pas de Session user OU que l'utilisateur
 // n'est pas un Admin, on le laisse pas rentrer !!!
 if (!isset($_SESSION['user']) || !$_SESSION['user']['isAdmin']) {
    header('Location: /');
    exit;
 }
 
 ?>


<?php include 'partials/header.php' ?>
<h1>Bienvenue dans la partie ADMIN !!</h1>
<?php include 'partials/footer.php' ?>