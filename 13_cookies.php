<?php 

// Vous pouvez créer des cookies sur le navigateur
// de votre visiteur


//Je peux vérifier qu'un cookie existe et l'utiliser

if (isset($_COOKIE['moncookie'])) {
    echo $_COOKIE['moncookie'];
} else {
    setcookie('moncookie', 'Je suis un VIRUS !!(non)', time() + 3600, '/');
}

// Pour supprimer un cookie, il suffit de le set à du vide
// setcookie('moncookie', "", -1, '/');
// Attention, gérer des cookies doit se faire en début de code
// AVANT d'afficher le moindre HTML

?>

<?php include "partials/header.php" ?>
<main>
    Coming soon ...
</main>
<?php include "partials/footer.php" ?>