# Mettre en place un "Watch mode"

## Installation des extensions

Avec VSCode
- Télécharger Live Server (si ce n'est pas déjà fait)

- Télécharger PHP Server

- Sur un navigateur Chrome, télécharger l'extension [Live Server Web Extension](https://chrome.google.com/webstore/detail/live-server-web-extension/fiegdmejfepffgpnejdinekhfieaogmj)

- Une fois l'extension installée, selectionnez là dans votre navigateur pour la configurer.
    - Activez le "live server"
    - Indiquez l'adresse de VOTRE serveur (http://localhost:8000/ si vous suivez cette procédure, mais cela peut changer)
    - Indiquez l'adresse de l'extension "Live-Server" (http://localhost:5500/ par defaut)

## Lancement

-Lancer Live server (bouton en bas de VSCode "Go Live")
    Cela ouvrira le port "5500" par défaut

-Lancer Php Server (clic droit sur votre projet puis "Php Server: Serve Project")
    Cela ouvrira le port "3000" par défaut

-Lancer VOTRE serveur
    ```
    php -S localhost:8000
    ```

Vous pouvez fermez les fenetres pointant sur localhost:5500 et localhost:3000, nous n'en avons pas besoin.

Si tout est lancé, vous devriez pouvoir travailler sur votre serveur local (:8000 dans notre cas) et chaque changement dans votre projet réactualisera la page !