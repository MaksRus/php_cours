<?php

/**
 * Les opérateurs de condition
 */

$a = 20;
$b = 10;

if ($a == $b) {
} // Egalité
if ($a === $b) {
} // Identique (égaux ET même type)
if ($a > $b) {
} // Plus grand que
if ($a >= $b) {
} // Plus grand ou égale
if ($a < $b) {
} // Plus petit que
if ($a <= $b) {
} // Plus petit ou égale
if ($a < $b && $a == 20) {
} // "ET" logique - les 2 conditions doivent être "vraies"
if ($a < $b || $a == 20) {
} // "OU" logique - il suffit qu'une des deux conditions soit "vraie"
if ($a != $b) {
} // Différent de
if (!$a) {
} // "NON" logique - il faut que $a soit "false"

// Tester l'existence ou l'état d'une variable
if ($a === null); // On test si $a est défini et vide
if (isset($a)); // On teste si $a a été défini ET est différente de NULL

$notes = [12, 43, 21];
if (empty($notes)); // On regarde si le tableau est vide


// Exemples
$age = 19;

if ($age >= 18) {
    echo "Vous avez le droit de rentrer";
} else {
    echo "C'est interdit aux mineurs ici !";
}

if (!empty($notes)) {
    echo "J'ai bien reçu les notes";
} else {
    echo "Impossible de calculer la moyenne : le tableau est vide...";
}

$month = date('m');
echo $month; // je suis un commentaire
if ($month >= 6 && $month <= 9) {
    echo "C'est l'été !";
} else {
    echo "C'est toujours pas l'été ...";
}
