<?php include 'partials/header.php' ?>

<main>
<?php // <--- on ouvre la balise "php"

    // Ecrire du texte
    echo "Bonjour tout le monde \n"; // Le \n indique un retour à la ligne
    echo "Coucou", 123, 25.6; // On peut passer plusieurs info si besoin

    // Semblable mais ne prend qu'un seul parametre... peu utilisé
    print "je suis printé";

    // Affiche le contenu d'un tableau
    print_r(['je suis le premier msg', 'et moi le 2eme']);

    // Affiche le contenu d'une variable, pratique pour débugger !
    var_dump(['je suis le premier msg', 'et moi le 2eme']);

    // Astuce : les print_r et var_dump rendent bien dans la console,
    // mais beaucoup moins dans du HTML.
    // Pour remédier à ce problème, vous pouvez encercler votre var_dump
    // dans des balises <pre></pre> !

    ?>
</main>
<?php include 'partials/footer.php' ?>