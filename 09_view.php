<?php include 'partials/header.php' ?>
<main>
    <h1>Je suis du HTML classique, je ne change jamais</h1>
    <?php
    echo "<h2>Moi par contre je suis généré en php !";

    // Quelques variables
    $name = "Marine";
    $age = 20;
    $specialClass = "important-red";
    $numbers = [1, 2, 3, 4, 5];
    $hero = [
        'name' => "Luke",
        'role' => "jedi",
        'isDarkSide' => false,
    ];

    // On peut écrire du php dans le tag <?php 
    // et faire de l'affichage avec les commandes
    // d'output classiques : echo, print_r, var_dump etc...
    echo "<p>Bonjour $name</p>";

    // Astuce : essayez toujours d'entourer vos print_r/var_dump
    // avec des balises <pre>, c'est beaucoup plus lisible pour debugger
    echo '<pre>';
    print_r($numbers);
    var_dump($hero);
    echo '</pre>';

    // On peut bien sûr utiliser les conditions
    if ($age >= 18) {
        echo "<p>Vous avez accès à tout notre contenu !</p>";
    } else {
        echo "<p>Tes parents savent que tu es là ...?</p>";
    }

    // Mais aussi les boucles !
    echo '<ul>';
    foreach ($numbers as $num) {
        echo $num;
    }
    echo '</ul>';

    //On remarque qu'il peut être assez compliqué
    //de mélanger HTML et PHP, on perd en lisibilité
    // et ce n'est pas très pratique à utiliser


    ?>

    <!-- // Voici quelques raccourcis : -->
    <!-- Attention, le < ?= ?> peut ne pas fonctionner sur certains serveurs ! -->
    <h2>Bonjour <?= $name ?></h2>
    <?php if ($age < 18) : ?>
        <h1>Vous avez accès à la zone jeu !</h1>
    <?php endif ?>
    <ul>
        <?php foreach ($numbers as $num) : ?>
            <li><?= $num ?></li>
        <?php endforeach ?>
    </ul>
    <!-- On peut aussi éditer l'intérieur des balises HTML avec des echo -->

    <article class="card <?= $specialClass ?>">
        Du contenu
    </article>
</main>
<?php include 'partials/footer.php' ?>