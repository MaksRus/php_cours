<?php 

// Toujours faire attention à ce qui vient
// d'un formulaire !

// Efforcez-vous de "filtrer" ce que l'on vous envoie !

if (isset($_POST['submit'])) {
    $name = $_POST['name']; // DANGEREUX !!!!
    $name = htmlspecialchars($_POST['name']); // Beaucoup plus sûr !

    // Version optimale qui propose plusieurs "filtres" possible (regardez la doc !)
    $name = filter_input(INPUT_POST, $_POST['name'], FILTER_SANITIZE_SPECIAL_CHARS); // DANGEREUX !!!!
    // Variante si c'est un "GET"
    $name = filter_input(INPUT_GET, $_GET['name'], FILTER_SANITIZE_SPECIAL_CHARS); // DANGEREUX !!!!
}


?>
<?php include "partials/header.php" ?>
<main>
    Coming soon ...
</main>
<?php include "partials/footer.php" ?>