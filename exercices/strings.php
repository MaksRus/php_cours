<?php

/**
 * Doit retourner un "extrait" d'une chaine de caractères.
 * Si la chaine est plus longue que 15 caractères, on ne
 * doit afficher que les 15 premiers suivis de "..."
 *
 * @param [type] $string
 * @return string
 */
function getExcerpt(string $string): string {
    return $string;
}

/**
 * Faire une fonction qui permet d'ajouter des 'fe' 
 * après chaque voyelle d'une chaîne de caractère,
 * et répéter la voyelle précédente, passée en paramètre
 * 
 * exmple : transform("Bonjour") => "Bofeonjofeoufeur"
 * @param string $string
 * @return string
 */
function transform(string $string): string {
    return "";
}

/**
 * Doit vérifier la validité d'un mot de passe.
 * Un mot de passe est valide si il
 * -contient 9 caractères ou plus
 * -contient le symbole "@"
 *
 * @param string $password
 * @return boolean
 */
function verifyPassword(string $password): bool
{
    return false;
}

