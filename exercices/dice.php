<?php

/**
 * Throw a number of dice
 * @param $nbDice
 * @return int[]
 */
function getRollDice($nbDice): array {
    return [];
}

/**
 * @param array $arrayDice
 */
function showDice(array $arrayDice): void {
}

function showResult(array $dice): void {
}

/**
 * Permet de déterminer si un lancé de dé est une suite ou non
 * A partir d'un tableau de solution
 *
 * @param $solutionStraight, le tableau de solution attendu
 * @param $dice, le lancé de dé
 * @param $result, le texte à afficher si la solution est valide
 * @return bool
 */
function isStraight($solutionStraight, $dice, $result): bool {
    return false;
}