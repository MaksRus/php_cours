<?php

/**
 * Doit retourner un "extrait" d'une chaine de caractères.
 * Si la chaine est plus longue que 15 caractères, on ne
 * doit afficher que les 15 premiers suivis de "..."
 *
 * @param [type] $string
 * @return string
 */
function getExcerpt(string $string): string {
    if (strlen($string) <= 15) {
        return $string;
    }

    return substr($string, 0, 15) . "...";
}

echo getExcerpt("Je suis un super long message !!!");

/**
 * Faire une fonction qui permet d'ajouter des 'fe' 
 * après chaque voyelle d'une chaîne de caractère,
 * et répéter la voyelle précédente, passée en paramètre
 * 
 * exmple : transform("Bonjour") => "Bofeonjofeoufeur"
 * @param string $string
 * @return string
 */
function transform(string $string): string {
    $voyelles = ['a', 'e', 'i', 'o', 'u', 'y', 'A', 'E', 'I', 'O', 'U', 'Y'];
    foreach($voyelles as $voyelle) {
        $string = str_replace($voyelle, $voyelle . "fe" . $voyelle , $string);
    }
    return $string;
}

echo transform("Bonjour");

/**
 * Doit vérifier la validité d'un mot de passe.
 * Un mot de passe est valide si il
 * -contient 9 caractères ou plus
 * -contient le symbole "@"
 *
 * @param string $password
 * @return boolean
 */
function verifyPassword(string $password): bool
{
    return strlen($password) >= 9 && str_contains($password, "@");
}

var_dump(verifyPassword("MonSuperMotDePasse@"));

