<?php

/**
 * La fonction doit retourner un deck de cartes
 * Chaque Carte possède une couleur (Coeur, Carreau, Pique, Trèfle)
 * ainsi qu'une valeur (A, 2, 3, 4, 5, 6, 7, 8, 9, 10, V, D, R)
 *
 * @return array
 */
function generateCardDeck(): array
{
    $deck = [];
    $colors = ["Coeur", "Carreau", "Pique", "Trèfle"];
    $values = ["A", "1", "2", "3", "4", "5", "6", "7", "8", "9", "V", "D", "R"];

    foreach($colors as $color) {
        foreach($values as $value) {
            $deck[] = [
                'color' => $color,
                'value' => $value
            ];
        }
    }

    return $deck;
}

/**
 * Permet de mélanger un deck de cartes de manière aléatoire.
 * Essayez de trouver comment faire "à la main" sans
 * utiliser "une certaine fonction" qui rendrait ça trop facile :p
 * 
 * @param array $deck
 * @return void
 */
function shuffleDeck(array $deck): array {
    $deckShuffled = [];
    while (count($deck) > 0) {
        $index = rand(0, count($deck) - 1);
        $deckShuffled[] = $deck[$index];
        array_splice($deck, $index, 1);
    }
    return $deckShuffled;
}

 /**
 * Version alternative sans "retour"
 * 
 * !Notez le "&" devant l'argument $deck : cela signifie que le $deck
 * passé sera véritablement modifié par la fonction.
 * function shuffleDeckAlternateVersion(array &$deck): void {}
 */


function drawCards(array &$deck, int $nb): array 
{
    $hand = [];
    while ($nb > 0) {
        $hand[] = $deck[0];
        array_splice($deck, 0, 1);
        $nb--;
    }
    return $hand;
}


/**
 * Distribue les cartes d'un jeu de tarot, uniquement pour 4 et 5 joueurs
 * @param int $nbPlayers
 * @param array $deck
 * @return array|null
 */
function dealCards(int $nbPlayers, array $deck): ?array {
    $iteratedPlayer = 0;
    $players = [];
    $doggo = [];
    while(count($deck) > 0) {
        // Le chien a 6 cartes à 4
        // Le chien a 3 cartes à 5
        while($iteratedPlayer < $nbPlayers) {
            if(!isset($players['Player ' . $iteratedPlayer])) {
                $players['Player ' . $iteratedPlayer] = [];
            }
            if ($nbPlayers == 4 && count($doggo) < 6
                || $nbPlayers == 5 && count($doggo) < 3
            ) {
                array_push($doggo[], ...drawCards($deck, 1));
            }
            array_push($players['Player ' . $iteratedPlayer], ...drawCards($deck, 3));
            $iteratedPlayer++;
        }
        $iteratedPlayer = 0;
    }
    $players['doggo'] = $doggo;
    return $players;
}
