<?php

/**
 * Supprime les doublons dans un tableau
 * Exemple :
 * [1, 2, 2, 3, 3, 3, 4, 5, 5]
 * => [1, 2, 3, 4, 5]
 * 
 * Il y a une méthode simple et une méthode complexe
 * @param array $tableau
 * @return void
 */
function deleteDuplicate(array $tableau)
{
    return array_unique($tableau); // Méthode facile

}

function deleteDuplicateHardMode(array $tableau)
{
    $tabloClean = [];

    foreach($tableau as $number) {
        if (!in_array($number, $tabloClean)) {
            array_push($tabloClean, $number);
        }
    }
    
    return $tabloClean;
}
$test = [1, 2, 2, 3, 3, 3, 4, 5, 5];

print_r(deleteDuplicate($test));
print_r(deleteDuplicateHardMode($test));