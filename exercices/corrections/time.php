<?php

/**
 * A partir d'un âge donné, retrouve l'année
 * de naissance
 * @param integer $age
 * @return integer
 */
function devineAnneeNaissance(int $age): int
{
    return 0;
}

/**
 * Prend en paramètre une durée en millisecondes.
 * Doit convertir cette durée au format texte.
 * Exemple:
 * Paramètre : 225000
 * Résultat : 03:45"00
 *
 * @param integer $milseconds
 * @return string
 */
function timeConverter(int $milliSecs): string
{
    print_r('Conversion de ' . $milliSecs . '...<br>');
    $ms = $milliSecs % 1000;
    $secondsMinutes = floor($milliSecs / 1000);
    $seconds = $secondsMinutes % 60;
    $minutes = floor($secondsMinutes / 60);
    $minutes = $minutes % 60;
    if ($minutes < 10) {
        $minutes = '0' . $minutes;
    }
    if ($ms < 10) {
        $ms = '00' . $ms;
    } else if ($ms < 100) {
        $ms = '0' . $ms;
    }
    if ($seconds < 10) {
        $seconds = '0' . $seconds;
    }
    return $minutes. ':' . $seconds . '"' . $ms;

}