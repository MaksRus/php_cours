<?php

/**
 * Prend un prix et une hausse/baisse (+20%, -20%...)
 * Retourne le nouveau prix après modification
 *
 * @param float $price
 * @param float $modifier
 * @return void
 */
function getNewPrice(float $price, float $modifier)
{
    return $price * $modifier;
}

$oldPrice = 199;
$newPrice = getNewPrice($oldPrice, 1.20); // Pour une augmentation de 20%
/**
 * A partir d'un tableau de notes,
 * calcule la moyenne des valeurs. 
 * @param integer $age
 * @return integer
 */
function getAverageGrade(array $notes): float
{
    return array_sum($notes) / count($notes);
}

/**
 * A partir d'un prix HT unitaire d'un produit
 * ainsi que de sa quantity, on veut connaitre le prix
 * total TTC arrondi à 2 chiffres après la virgule.
 * (On part du principe que la TVA est de 20%)
 * @param float $priceHT
 * @param integer $quantity
 * @return void
 */
function getPriceTTC(float $priceHT, int $quantity, float $tva = 1.20): float
{
    return $priceHT * $quantity * $tva;
}

/**
 * Retourne l'état d'un volume d'eau selon sa température.
 * Positif : liquide
 * Négatuf : solide
 * Au dela de 70° : gazeux
 * @param float $temperature
 * @return string
 */
function getWaterState(float $temperature): string
{
    if ($temperature > 70) {
        return "gazeux";
    } else if ($temperature < 0) {
        return "solide";
    }
    // Même pas besoin du "else" puisque les "return" font sortir de la fonction
    return "liquide";
    
}

/**
 * Prend en paramètre une classe et affiche
 * pour chaque élève son nom et sa moyenne
 * 
 * Données :
 * Albert : 12, 8, 9, 7, 13
 * Michel : 14, 13, 12, 11, 10
 * Vincent : 17, 16, 15, 18, 13
 * 
 * Première étape : trouver comment formatter ces données
 * pour les rendre facile à manipuler.
 * @param array $class
 * @return void
 */
function getAverageGradeByStudent(array $class)
{
    foreach($class as $name => $notes) {
        echo "$name a obtenu la moyenne de " . getAverageGrade($notes) . "/20", "\n";
    }
}

$class = [
  "Albert" => [ 12, 8, 9, 7, 13 ],
  "Michel" => [ 14, 13, 12, 11, 10 ],
  "Vincent" => [ 17, 16, 15, 18, 13 ],
];

getAverageGradeByStudent($class);

/**
 * Doit afficher, ligne par ligne, une table de multiplication
 *  Par exemple, si je donne 5 en multiplier et 3 en max:
 *  5x1 = 5
 *  5x2 = 10
 *  5x3 = 15
 * 
 * @param [type] $multiplier
 * @param [type] $max
 * @return void
 */
function maNouvelleFonction($multiplier, $max): void {
    for ($i = 1; $i <= $max; $i++) {
        echo $multiplier . "x" . $i . " = " . $multiplier * $i, "\n";
    }
}

displayMultiplyTable(7, 10);