<?php

/**
 * Prend un prix et une hausse/baisse (+20%, -20%...)
 * Retourne le nouveau prix après modification
 *
 * @param float $price
 * @param float $modifier
 * @return void
 */
function getNewPrice(float $price, float $modifier)
{
    return 0;
}

/**
 * A partir d'un tableau de notes,
 * calcule la moyenne des valeurs. 
 * @param integer $age
 * @return integer
 */
function getAverageGrade(array $notes): float
{
    return 0;
}

/**
 * A partir d'un prix HT unitaire d'un produit
 * ainsi que de sa quantity, on veut connaitre le prix
 * total TTC arrondi à 2 chiffres après la virgule.
 * (On part du principe que la TVA est de 20%)
 * @param float $priceHT
 * @param integer $quantity
 * @return void
 */
function getPriceTTC(float $priceHT, int $quantity)
{
    return 0;
}

/**
 * Retourne l'état d'un volume d'eau selon sa température.
 * Positif : liquide
 * Négatuf : solide
 * Au dela de 70° : gazeux
 * @param float $temperature
 * @return string
 */
function getWaterState(float $temperature): string
{
    return "";
}

/**
 * Prend en paramètre une classe et affiche
 * pour chaque élève son nom et sa moyenne
 * 
 * Données :
 * Albert : 12, 8, 9, 7, 13
 * Michel : 14, 13, 12, 11, 10
 * Vincent : 17, 16, 15, 18, 13
 * 
 * Première étape : trouver comment formatter ces données
 * pour les rendre facile à manipuler.
 * @param array $class
 * @return void
 */
function getAverageGradeByStudent(array $class)
{
    return null;
}

/**
 * Doit afficher, ligne par ligne, une table de multiplication
 *  Par exemple, si je donne 5 en multiplier et 3 en max:
 *  5x1 = 5
 *  5x2 = 10
 *  5x3 = 15
 * 
 * @param [type] $multiplier
 * @param [type] $max
 * @return void
 */
function displayMultiplyTable($multiplier, $max): void {
}

