<?php

/**
 * La fonction doit retourner un deck de cartes
 * Chaque Carte possède une couleur (Coeur, Carreau, Pique, Trèfle)
 * ainsi qu'une valeur (A, 2, 3, 4, 5, 6, 7, 8, 9, 10, J, D, R)
 *
 * @return array
 */
function generateCardDeck(): array
{
    return [];
}

/**
 * Permet de mélanger un deck de cartes de manière aléatoire.
 * Essayez de trouver comment faire "à la main" sans
 * utiliser "une certaine fonction" qui rendrait ça trop facile :p
 * 
 * @param array $deck
 * @return void
 */
function shuffleDeck(array $deck): array 
{
    return [];
}

 /**
 * Version alternative sans "retour"
 * 
 * !Notez le "&" devant l'argument $deck : cela signifie que le $deck
 * passé sera véritablement modifié par la fonction.
 * function shuffleDeckAlternateVersion(array &$deck): void {}
 */

/**
 * Permet de "piocher" un certain nombre de carte.
 * Il faut que les cartes piochés "disparaissent" du deck
 * et qu'elles se retrouvent dans la "main" du joueur.
 * 
 * On doit pouvoir préciser le nombre de carte à piocher.
 *
 * @param array $deck
 * @param integer $nb
 * @return array
 */
function drawCards(array &$deck, int $nb): array 
{
    return [];
}

/**
 * Distribue les cartes d'un jeu de tarot, uniquement pour 4 et 5 joueurs
 * @param int $nbPlayers
 * @param array $deck
 * @return array|null
 */
function dealCards(int $nbPlayers, array $deck): ?array 
{
    return [];
}
