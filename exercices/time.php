<?php

/**
 * A partir d'un âge donné, retrouve l'année
 * de naissance.
 * 
 * (Sans utiliser d'objet !! Donc pas de "DateTime" !)
 * @param integer $age
 * @return integer
 */
function devineAnneeNaissance(int $age): int
{
    return 0;
}

/**
 * Prend en paramètre une durée en millisecondes.
 * Doit convertir cette durée au format texte.
 * Exemple:
 * Paramètre : 225000
 * Résultat : 03:45"00
 *
 * @param integer $milseconds
 * @return string
 */
function timeConverter(int $milliSecs): string
{
    return "00:00:00";
}