<?php
/**
 * Créé un Héro (tableau associatif) au format suivant:
 * HERO :
 *     name: "Warrior"
 *     hp: 300
 *     strength: 45
 * 
 * !! A vous de trouver les arguments dont la fonction à besoin
 * @return array
 */
function createHero(): array
{
    return [];
}

/**
 * Affiche les informations d'un héros de
 * manière compréhensible pour l'utilisateur français :
 * Nom : "Hercule"
 * Points de Vie: 500
 * Force: 45
 *
 * @param array $hero
 * @return void
 */
function getHeroInfo(array $hero): void
{

}

/**
 * Affiche les informations d'un groupe de héros.
 *
 * @param array $heroes
 * @return void
 */
function getHeroesInfo(array $heroes)
{

}

/**
 * Un héros inflige d'habitude exactement
 * sa force "attack" en dégats.
 * Cependant, les héros ont 15% de chance d'infliger
 * 150% dégats (coup critique).
 * 
 * @param integer $strength
 * @return float
 */
function getDamage(int $strength): float
{
    return 0;
}

/**
 * Indique si le héro passé en argument
 * est toujours en vie.
 *
 * Doit retourner vrai ou faux
 * @param array $hero
 * @return boolean
 */
function isAlive(array $hero): bool
{
    return false;
}

// Le & correspond à un passage par référence, c'est à dire que l'on
// veut modifier la variable à l'intérieur de la fonction
function dealDamages(array $attaquant, array &$cible) {
}


/**
 * Fait s'affronter deux groupes de "héros".
 * 
 * - Un héros inflige des blessures à l'autre selon son "attack"
 * - Les blessures font perdre des "hp" (points de vie)
 * - Un héros qui n'a plus de "hp" ne peut plus se battre
 * - Tant qu'un des deux groupes n'a pas été décimé, on fait combattre les
 * héros chacun leur tour. 
 *
 * @return void
 */
function arena(array $heroes, array $opponents): void
{

}